local _M = {}

_M.sound = {
	button_click = audio.loadSound( "sounds/button-click.wav" ),
	progress = audio.loadSound( "sounds/progress.wav" ),
	wrong = audio.loadSound( "sounds/wrong.wav" ),
	right = audio.loadSound( "sounds/right.wav" ),
	top_score = audio.loadSound( "sounds/top-score.wav" ),
	new_live = audio.loadSound( "sounds/new-live.wav" ),
}

_M.channel = {
	COMMON_CHANNEL = 1,
	SFX_CHANNEL = 2,
	PLAYER_CHANNEL = 3,
	MUSIC_CHANNEL = 4,
}

function _M.play( sound, channel, loops)
	-- utils.debug('game:play_sound', loops)
	if settings.sounds then
		if channel == nil then channel = _M.channel.COMMON_CHANNEL end
		if loops == nil then loops = 0 end

		-- utils.debug('game:play_sound', channel)

		audio.stop( channel )

		audio.play( sound, {channel = channel, loops = loops} )
	end
end

function _M.stop( channel )
	if settings.sounds then
		audio.stop( channel )
	end
end

function _M.vibrate()
	if settings.vibrations then
		system.vibrate()
	end
end

return _M