local native = require( 'native' )
local composer = require( "composer" )

local _M = {}

function _M.on_key_event( event )
	local phase = event.phase
	local keyName = event.keyName

	if ( "back" == keyName and phase == "up" ) then
		for id, value in pairs(timer._runlist) do
			timer.cancel(value)
		end
		
		-- handle button
		if composer.getSceneName( "current" ) == 'menu' then
			native.requestExit()
			return true
		else
			composer.gotoScene('menu')
			return true
		end
	end
end

function _M.init( ... )
	if platform_name == 'Android' then
		Runtime:addEventListener( "key", _M.on_key_event )
	end
end

return _M