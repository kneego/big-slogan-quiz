local json = require('json')
local gameNetwork = require( 'gameNetwork' )

local _M = {}

-- logged in status
_M.logged_in = false

-- player data
-- playerID (string)
-- alias (string)
-- isFriend (boolean)
-- isAuthenticated (boolean)
-- isUnderage (boolean)
-- friends (array)
_M.load_local_player_automatically = true
_M.load_local_player_callback = nil

_M.debugging = false

_M.platform_name = platform_name
_M.init_error_message = 'Error logging in.\nTry it later.'

function _M._default_init_callback( event )
	if event and (event.errorCode or event.isError) then
		-- something went wrong
		if _M.init_error_message then
			native.showAlert( 'Network error', _M.init_error_message, {'OK'} )
		end

		_M.logged_in = false

		return false
	end

	if event and (event.data or is_android) then
		-- user is logged in
		_M.logged_in = true

		-- load user info
		if _M.load_local_player_automatically then
			_M.load_local_player( _M.load_local_player_callback )
		end

		if callback ~= nil then
			callback(event)
		end
	end
end

function _M.init(callback, user_initiated)
	if _M.debugging then
		return
	end

	local _service_name = ''

	local _user_initiated
	if user_initiated ~= nil then _user_initiated = user_initiated else _user_initiated = true end

	local _callback
	if callback ~= nil then _callback = callback else _callback = _M._default_init_callback end

	if _M.platform_name == 'iOS' then
		_service_name = 'gamecenter'
	elseif _M.platform_name == 'Android' then
		_service_name = 'google'
	end

	if _service_name ~= '' then
		local data = gameNetwork.init( _service_name, _callback )

		if not data then
			if is_android then
				-- log in user
				_M.logged_in = false

				gameNetwork.request("login",
				{
					userInitiated = _user_initiated,
					listener = _callback
				})
			else
				-- user is logged in to game center
				_M.logged_in = true

				if _callback ~= nil then
					_callback()
				end
			end
		end
	end
end

function _M.send_score(board_id, score, callback)
	if _M.debugging then
		utils.debug('game_network:send_score', board_id, score)
		return
	end

	if board_id == nil then return end

	if _M.logged_in then
		gameNetwork.request( "setHighScore", {
			localPlayerScore = { category=board_id, value=score },
			listener = callback
		})
	end
end

function _M.show_leaderboards()
	if _M.debugging then
		utils.debug('game_network:show_leaderboards')
		return
	end

	if not _M.logged_in then
		_M.init()
	end

	if _M.logged_in then
		gameNetwork.show( 'leaderboards', { 
			leaderboard = {timeScope='Week'},
		} )
	end
end

function _M.unlock_achievement(identifier, callback)
	if _M.debugging then
		utils.debug('game_network:unlock_achievement')
		return
	end

	if identifier == nil then return end

	if _M.logged_in then
		gameNetwork.request( "unlockAchievement",
			{
				achievement =
				{
					identifier = identifier
				},
				listener = callback
			}
		)
	end
end

function _M.load_local_player( callback )
	if callback == nil then return end

	if _M.debugging then
		utils.debug('game_network:load_local_player')
		return
	end

	-- tento request spravim neskor, aby som mal istotu, ze user je nalogovany
	timer.performWithDelay( 10000, function ( ... )
		if _M.logged_in then
			gameNetwork.request( "loadLocalPlayer",
				{
					listener = callback
				}
			)
		end
	end )
end

return _M