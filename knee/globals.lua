-- globals.lua

local system = require( 'system' )
local display = require( 'display' )

screenW = display.contentWidth
screenH = display.contentHeight

halfW = display.contentWidth * 0.5
halfH = display.contentHeight * 0.5
display_ar = screenH / screenW

language = system.getPreference( 'locale', 'language')

platform_name = system.getInfo('platformName')
app_name = system.getInfo( 'appName' )
app_version = tonumber( system.getInfo( 'appVersionString' ) ) or 0
device_id = system.getInfo( 'deviceID' )

if platform_name == 'iPhone OS' then platform_name = 'iOS' end

is_android = (platform_name == 'Android')
is_ios = (platform_name == 'iOS')
is_emulator = (platform_name == 'Mac OS X')

if is_emulator then platform_name = 'iOS' end