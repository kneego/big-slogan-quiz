local native = require( 'native' )
local system = require( 'system' )

local _M = {}

_M.debugging = false

_M.store = nil
_M.googleIAP = false

_M.purchase_ok = nil
_M.purchase_failed = nil
_M.purchase_cancelled = nil

_M.consume = false


function _M.init()
	utils.debug('iap:init', '1')
	if _M.debugging then
		utils.debug('iap:init', 'debug enabled')
		return
	end

	local googleIAP = false

	if is_android then
	    _M.store = require( 'plugin.google.iap.v3' )
	    _M.store.init( 'google', _M.transaction_callback )
	    _M.googleIAP = true
	elseif is_ios then
	    _M.store = require( 'store' )
	    _M.store.init( 'apple', _M.transaction_callback )
	else
	    native.showAlert( 'Notice', 'In-app purchases are not supported.', { 'OK' } )
	    return
	end

	-- check if store is active
	_M.isActive = _M.store.isActive
	_M.canLoadProducts = _M.store.canLoadProducts
end

function _M.transaction_callback( event )
	local transaction = event.transaction

	if transaction.state == 'purchased' then
		-- for android consume this product
		if is_android and _M.consume then
			_M.store.consumePurchase( transaction.productIdentifier )
		end

		if _M.purchase_ok ~= nil then
			_M.purchase_ok( transaction )
		else
			utils.debug('iap:transaction_callback', 'purchase_ok is not defined')
		end
	elseif transaction.state == 'cancelled' then
		if _M.purchase_cancelled ~= nil then
			_M.purchase_cancelled( transaction )
		else
			native.showAlert( 'Notice', 'In-app purchase was cancelled.', { 'OK' } )
		end
	elseif transaction.state == 'failed' then
		if is_android and transaction.errorType == 7 then
			if _M.purchase_item_owned ~= nil then
				_M.purchase_item_owned( transaction )
			else
				native.showAlert( 'Notice', 'Item already owned.', { 'OK' } )
			end
		elseif _M.purchase_failed ~= nil then
			_M.purchase_failed( transaction )
		else
			native.showAlert( 'Notice', 'In-app purchase failed.', { 'OK' } )
		end
	elseif transaction.state == 'restored' then
		if _M.purchase_restored ~= nil then
			_M.purchase_restored( transaction )
		else
			native.showAlert( 'Notice', 'In-app purchases restored.', { 'OK' } )
		end
	end

	_M.store.finishTransaction( transaction )
end

function _M.load_products_callback( event )
	-- default load callback
	utils.debug('iap:load_products_callback', #event.products)

	for i = 1, #event.products do
		local item = event.products[i]

		for k, v in pairs(item) do
			utils.debug('iap:load_products_callback', 'product: ', i, '; property: ', k, '; value: ', v)
		end
	end
end

function _M.load_products( items )
	if _M.debugging then
		utils.debug('iap:load_products', 'debug enabled')
		return
	end

	if not _M.canLoadProducts then
		utils.debug('iap:load_products', 'iap can not load products')
		return
	end

	if type( items ) ~= 'table' then
		items = { items }
	end

	_M.store.loadProducts( items, _M.load_products_callback )
end

function _M.purchase( items )
	if _M.debugging then
		utils.debug('iap:purchse', 'debug enabled')
		return
	end

	if not _M.isActive then
		utils.debug('iap:purchse', 'iap is not active')
		return
	end

	if is_ios and type( items ) ~= 'table' then
		items = { items }
	end

	_M.store.purchase( items )
end

function _M.restore( items )
	if _M.debugging then
		utils.debug('iap:restore', 'debug enabled')
		return
	end

	if not _M.isActive then
		utils.debug('iap:restore', 'iap is not active')
		return
	end

	_M.store.restore()
end

return _M