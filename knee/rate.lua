-- rate us dialog
local os = require( 'os' )
local math = require( 'math' )
local system = require( 'system' )

local utils = require( 'knee.utils' )
local globals = require( 'knee.globals' )


local _M = {}

function _M.init( config )
	-- default parametre
	_M.config = {
		android_rate = '', --link to rate Android app
		ios_rate = '',     --link to rate IOS app
		times_used = 15,   --times to use before asking to rate
		days_used = 30,    --days to use before asking to rate
		version = 0,      --current version of the app
		remind_times = 5,  --times of use to wait before reminding
		remind_days = 5,    --days of use to wait before reminding
		rate_title = 'Rate My App',
		rate_text = 'Please rate my app',
		rate_button = 'Rate it now!',
		remind_button = 'Remind me later',
		cancel_button = 'No, thanks',
	}

	if config then
		for key, value in pairs( config ) do
			_M.config[key] = value
		end
	end

	-- loadneme ulozene settingy
	_M.settings = utils.loadSettings({
		path = 'rate.json',
		emptyData = _M.get_new_data(),
	})
end

function _M.to_rate()
	if _M.settings == nil or _M.settings.version < _M.config.version then
		-- appku pouziva prvy krat alebo ma novu verziu
		_M.reset()
	elseif _M.settings.to_rate then
		_M.rate()
	end
end

function _M.rate()
	_M.settings.times_used = _M.settings.times_used + 1
	if _M.settings.times_used >= _M.settings.times_to_wait then
		_M.show()
	else
		local now = os.time()
		local days = math.floor((((now - _M.settings.start_date) / 60) / 60) / 24)
		if days >= _M.settings.days_to_wait then
			_M.show()
		end
	end
	
	-- ulozime settingy
	_M.save_settings()
end

function _M.open_rate_url()
	_M.settings.to_rate = false

	-- ulozime settingy
	_M.save_settings()

	if is_android and _M.config.android_rate ~= '' then
		system.openURL( _M.config.android_rate )
	elseif is_ios and _M.config.ios_rate ~= '' then
		system.openURL( _M.config.ios_rate )
	end
end

function _M.dialog_callback( event )
	if event.index == 1 then
		-- rate
		_M.open_rate_url()
	elseif event.index == 2 then
		-- remind later
		_M.settings.days_to_wait = _M.settings.days_to_wait + _M.config.remind_days
		_M.settings.times_to_wait = _M.settings.times_to_wait + _M.config.remind_times
		
		_M.settings.to_rate = true

		-- ulozime settingy
		_M.save_settings()
	else
		-- no, thanks
		_M.settings.to_rate = false

		-- ulozime settingy
		_M.save_settings()
	end
end

function _M.save_settings()
	-- ulozime settingy
	utils.saveSettings( _M.settings, {
		path = 'rate.json',
	})
end

function _M.show()
	-- zobrazime rate dialog
	local alert = native.showAlert( _M.config.rate_title, _M.config.rate_text, { _M.config.rate_button, _M.config.remind_button, _M.config.cancel_button }, _M.dialog_callback )
end

function _M.get_new_data()
	return {
		start_date = os.time(),
		times_used = 1,
		version = _M.config.version,
		days_to_wait = _M.config.days_used,
		times_to_wait = _M.config.times_used,
		to_rate = true
	}
end

function _M.reset()
	-- resetneme countre
	_M.settings = _M.get_new_data()

	-- ulozime settingy
	_M.save_settings()
end

return _M