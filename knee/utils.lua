--
-- Part of KneeGo useful lib
--
-- Pavel Koci (@pavelkoci)
-- KneeGo (@KneeGoApps)
--
-- https://bitbucket.org/pavelkoci/corona-knee-lib/
--

local io = require( "io" )
local json = require( "json" )
local timer = require( "timer" )
local system = require( "system" )
local native = require( "native" )
local composer = require( "composer" )
local gameNetwork = require( "gameNetwork" )

local math = require( "math" )
math.randomseed( os.time() )

debugging = false

local M = {}

function M.debug( ... )
	if debugging then
		print ('debug:', unpack(arg))
	end
end

function M.debug_json( data )
	if debugging then
		print ('debug:', json.encode(data))
	end
end

function M.warn( ... )
	print ('warn:', unpack(arg))
end

function M.handlerAdd(object, event, handler)
	object[event] = handler
	object:addEventListener(event)
end

function M.handlerRemove(object, event, handler)
	object:removeEventListener(event, handler)
end

function M.nextFrame(f)
	timer.performWithDelay( 1, f )
end

function M.loadSettings( options )
	local customOptions = options or {}

	local opt = {}
	opt.path = customOptions.path or 'settings.json'
	opt.emptyData = customOptions.emptyData

	local path = system.pathForFile( opt.path, system.DocumentsDirectory )
	local file = io.open( path, "r" )

	if file then
		-- load state
		local savedDate = file:read( "*a" )
		
		io.close( file )
		
		file = nil

		return json.decode( savedDate )
	else
		-- create empty data and save it
		local emptyData
		if opt.emptyData and type(empty) == 'function' then
			emptyData = opt.emptyData()
		elseif opt.emptyData then
			emptyData = opt.emptyData
		else
			emptyData = {}
		end

		M.saveSettings(emptyData, {
			path = opt.path
		})

		return emptyData
	end
end

function M.saveSettings( data, options )
	local customOptions = options or {}

	local opt = {}
	opt.path = customOptions.path or 'settings.json'
	
	local jsonData = json.encode( data )

	local path = system.pathForFile( opt.path, system.DocumentsDirectory )
	local file = io.open( path, "w" )

	file:write( jsonData )

	io.close( file )

	file = nil
end

function M.test_network_connection()
    local netConn = require('socket').connect('www.apple.com', 80)
    
    if netConn == nil then
        return false
    end

    netConn:close()
    return true
end

function M.color(r, g, b)
	if type(r) == 'table' then
		g = r[2]
		b = r[3]
		r = r[1]
	end

	return r / 255, g / 255, b / 255
end

function M.get_fonts_names()
	if M.debugging ~= true then return end

	local fonts = {}
	
	local systemFonts = native.getFontNames()

	for i, fontName in ipairs( systemFonts ) do
		M.debug( 'font:', string.lower(fontName))
		fonts[#fonts + 1] = string.lower(fontName)
	end
end

return M