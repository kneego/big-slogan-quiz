local M = {}

M.EMBOSS_COLOR = {
	highlight = { r = 32 / 255, g = 70 / 255, b = 9 / 255 },
	shadow = { r = 32 / 255, g = 70 / 255, b = 9 / 255 }
}

M.DEFAULT_FONT = 'BreeSerifRegular'
M.MENU_FONT = 'NunitoRegular'
M.ITALIC_FONT = 'AmiriBoldSlanted'
M.AWESOME_FONT = 'fontawesome'

M.COLOR_BUTTON_BG = { 232, 44, 91 }
M.COLOR_BUTTON_BG_BOTTOM = { 136, 9, 33 }
M.COLOR_TEXT_COLOR = { 255, 255, 255 }
M.COLOR_BG = { 41, 60, 78 }

M.RED_COLOR = { 206, 20, 53 }
M.WHITE_COLOR = { 235, 232, 220 }
M.BLUE_COLOR = { 106, 181, 177 }
M.GRAY_COLOR = { 108, 106, 97 }

M.BLUE_BUTTON_FILL = {
	type = 'gradient',
	color1 = {utils.color(164, 214, 212)},
	color2 = {utils.color(108, 183, 178)},
	direction = 'down'
}

M.RED_BUTTON_FILL = {
	type = 'gradient',
	color1 = {utils.color(237, 50, 100)},
	color2 = {utils.color(208, 22, 57)},
	direction = 'down'
}


function M.button_down( button )
	button.y = button.y + 1
	button.down = true

	display.currentStage:setFocus( button )

	sounds.play(sounds.sound.button_click)
end

function M.button_up( button )
	if button.down then
		button.y = button.y - 1
		button.down = false

		display.currentStage:setFocus( nil )

		return true
	else
		return false
	end
end

function M.background()
	local background_width = 720
	local background_height = 1280
	local height = (background_height / background_width) * screenW
	
	local background = display.newImageRect( 'images/background.jpg', screenW, height )
	background.anchorX = 0
	background.anchorY = 0
	background.y = 0
	background.x = 0

	return background
end

function M.background_footer()
	local background_width = 720
	local background_height = 19
	local height = (background_height / background_width) * screenW
	
	local background_footer = display.newImageRect( 'images/background-footer.png', screenW, height )
	background_footer.alpha = 0
	background_footer.x = halfW
	background_footer.y = screenH
	background_footer.anchorY = 1

	return background_footer
end

function M.button_background(x, y, w, h, fill)
	local button_group = display.newGroup()

	local button_bg_bottom_color = M.COLOR_BUTTON_BG_BOTTOM
	local button_fill = M.RED_BUTTON_FILL

	if fill == 'blue' then
		button_bg_bottom_color = { 73, 136, 133 }
		button_fill = M.BLUE_BUTTON_FILL
	end

	local button_bg_bottom = display.newRoundedRect( x, y, w, h, 5 )
	button_bg_bottom:setFillColor( utils.color( button_bg_bottom_color ) )
	button_group:insert( button_bg_bottom )

	local button_bg = display.newRoundedRect( x, y - 2, w, h - 4, 5 )
	button_bg.fill = button_fill
	button_group:insert( button_bg )
	
	return button_group
end

function M.button( data )
	local button_group = display.newGroup()

	local button_bg = M.button_background(data.x, data.y, data.w, data.h, data.fill)
	button_group:insert(button_bg)

	local button_label = display.newEmbossedText({
		text = data.label,
		font = data.font or M.DEFAULT_FONT,
		fontSize = data.font_size or 20,
		x = data.x,
		y = data.y - 2 + (data.label_offset or 0)
	})
	button_label:setFillColor(utils.color(M.COLOR_TEXT_COLOR))
	button_group:insert( button_label )

	utils.handlerAdd( button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			M.button_down( button_group )
		elseif event.phase == 'ended' and M.button_up( button_group ) then
			data.handler()
		end

		return true
	end )

	button_group.button_bg = button_bg
	button_group.button_label = button_label

	return button_group
end

M.show_ad_box_callback = nil
function M.ad_listener( event )
	if event.isError then
		M.hide_ad_box()
	else
		if M.show_ad_box_callback ~= nil then
			M.show_ad_box_callback( ads.height() )
		end
	end
end

M.hide_ad_box_callback = nil
function M.hide_ad_box()
	ads.hide()
	
	if M.hide_ad_box_callback ~= nil then
		M.hide_ad_box_callback()
	end
end

function M.format_time( miliseconds )
	local seconds = math.floor(miliseconds / 1000)
	local ms = math.floor((miliseconds / 1000 - seconds) * 10)

	if seconds < 10 then 
		seconds = '0' .. seconds
	end

	return seconds .. '.' .. ms
end

return M