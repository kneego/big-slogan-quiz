require( 'knee.globals' )

-- data = require( "data" )
ads = require( 'ads' )
rate = require( 'knee.rate' )
utils = require( 'knee.utils' )
back_button = require( 'knee.back_button' )

require( 'common' )

display.setStatusBar( display.HiddenStatusBar )
display.setDefault( 'background', 0, 0, 0 )
display.setDefault( 'fillColor', utils.color(255, 255, 255) )

if audio.supportsSessionProperty == true then
	audio.setSessionProperty(audio.MixMode, audio.AmbientMixMode)
end

debugging = false
-- utils.debugging = debugging

if debugging then
	require( 'knee.screen_capture' )  -- aby sme vedeli robit screenshoty (klaves S)
	utils.get_fonts_names()  -- aby sme vedeli nazvy fontov

	-- language = 'sk'  -- testujeme rozne jazyky
end

game_network = require( 'knee.game_network' )
game_network.debugging = false

game_network.load_local_player_callback = function (event)
	if event ~= nil and event.data ~= nil then
	end
end

timer.performWithDelay( 1500, function ( ... )
	game_network.init()
end )


-- resources (strings, arrays)
R = require( 'resources' )
sounds = require('sounds')

ga = require( 'knee.ga' )
ga:init( 'UA-39791763-29' )

local common = require( 'common' )
ads.init( 'admob', R.arrays('admob_id_banner')[platform_name], common.ad_listener)

back_button.init()

-- load settings
settings = utils.loadSettings({
	-- default empty data
	emptyData = {
		total_time = 0,
		sounds = true,
		ads_removed = false,
		stats = {
			{
				unlocked = true,
				successfull = false,
				time = 0,
				stats = 0
			}
		}
	}
})

-- build levels
all_slogans = R.arrays('slogans')
slogan_categories = {}
slogan_levels = {}
for i = 1, #all_slogans do
	local item = all_slogans[i]

	if slogan_levels[tonumber(item.level)] == nil then slogan_levels[tonumber(item.level)] = {} end
	table.insert(slogan_levels[tonumber(item.level)], item)

	if slogan_categories[item.category] == nil then slogan_categories[item.category] = {} end
	table.insert(slogan_categories[item.category], item)
end

-- load menu screen
local composer = require( 'composer' )

composer.gotoScene( 'menu' )
