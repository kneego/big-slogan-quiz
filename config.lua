application = {
	content = {
		width = 320,
		height = 480, 
		scale = "letterBox",
		fps = 30,
		
        imageSuffix = {
		    ["@2x"] = 1.42857143,
		}
	},

    license = {
        google = {
            key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApmk/zE4MXnKGo5EzeASwhuUjqg+RVv7Q4tyYWiiyL8UQ9QPmShBo1Q3q7AAqrXijVUXLpzCs3JxKmGbfjURBY+O6/tG9rANHRFPMsxD4IXh5oJnymhUSb0Lt7ocDNm+nxwN7bojAB5MOMJQ9PkyNKt82U+hXeFLvg16YZW0U3IHgV15JRW6kGnWvNpsRwp5DHEjgBIJSy79yCyzb28smedTA8TlZS71pM8NFYhpz1n7lbtLu0CCyF+cvG7/NeJImsWTgyhBCZ9tPsOu+eXjPy/FxdmHdsACzK1bbyxE2+bjzdvTRw+CsKGmsE9DhQDc2CAqOVSPadDXf5YoGWNiREwIDAQAB",
        },
    },
}

local _ar = display.pixelHeight / display.pixelWidth
if _ar > 1.4 then
    application['content']['height'] = 320 * _ar
else
    -- ipad
    application['content']['width'] = 360
    application['content']['height'] = 360 * _ar
end