-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( 'composer' )
local scene = composer.newScene()

local widget = require( 'widget' )
local common = require( 'common' )

--------------------------------------------

-- forward declarations and other locals
local main_group
local background_footer
local levels = R.arrays('levels')

local sounds_button
local table
local reload = false


function game_level_button( data )
	local button_group = display.newGroup( )
	
	if data.unlocked then
		local color = {utils.color(206, 20, 53)}
		if data.successfull then color = {utils.color(123, 193, 191)} end

		local shadow = display.newRoundedRect( halfW + 1, 1, screenW - 30, 46, 5 )
		shadow:setFillColor( 0.8 )
		shadow.alpha = 0.6
		button_group:insert( shadow )

		local button = display.newRoundedRect( halfW, 0, screenW - 30, 46, 5 )
		button_group:insert( button )

		local level_label = display.newText( button_group, 'Level', 35, -1, common.DEFAULT_FONT, 14 )
		level_label.anchorX = 0
		level_label.anchorY = 0.5
		level_label:setFillColor( unpack(color) )

		local level_number = display.newText( button_group, data.level, 80, -3, common.DEFAULT_FONT, 24 )
		level_number.anchorX = 0
		level_number.anchorY = 0.5
		level_number:setFillColor( unpack(color) )

		local start_position = screenW - 200

		local clock_icon = display.newImageRect( button_group, 'images/clock.png', 23, 23 )
		clock_icon.anchorX = 0
		clock_icon.x = start_position
		clock_icon.y = 0

		local clock_content = display.newText( button_group, common.format_time( data.time ), start_position + 24, -1, common.DEFAULT_FONT, 14 )
		clock_content.anchorX = 0
		clock_content.anchorY = 0.5
		clock_content:setFillColor( 0 )

		local badge_icon = display.newImageRect( button_group, 'images/badge.png', 23, 23 )
		badge_icon.anchorX = 0
		badge_icon.x = start_position + 54
		badge_icon.y = 0

		local badge_content = display.newText( button_group, data.score or 0, start_position + 76, -1, common.DEFAULT_FONT, 14 )
		badge_content.anchorX = 0
		badge_content.anchorY = 0.5
		badge_content:setFillColor( 0 )

		local check_icon = display.newImageRect( button_group, 'images/check.png', 23, 23 )
		check_icon.anchorX = 0
		check_icon.x = start_position + 100
		check_icon.y = 0

		local check_content = display.newText( button_group, data.stats_text, start_position + 124, -1, common.DEFAULT_FONT, 14 )
		check_content.anchorX = 0
		check_content.anchorY = 0.5
		check_content:setFillColor( unpack(color) )

		utils.handlerAdd( button_group, 'touch', function ( self, event )
			if event.phase == 'began' then
				common.button_down( button_group )
			elseif event.phase == 'ended' and common.button_up( button_group ) then
				data.handler( data.level )
			elseif event.phase == 'moved' then
				if math.abs( event.y - event.yStart ) > 3 then
					common.button_up( button_group )

					display.getCurrentStage():setFocus( nil )
	                event.target.isFocus = false                 
	                event.target = table._view
	                event.phase = "began"
	                table._view.touch( table._view, event )
	            end
			end

			return true
		end )
	else
		local button = display.newRoundedRect( halfW, 0, screenW - 30, 46, 5 )
		button:setFillColor( 0, 0, 0, 0 )
		button.strokeWidth = 1
		button:setStrokeColor( 0.8 )
		button.alpha = 0.4
		button_group:insert( button )

		local level_label = display.newText( button_group, 'Level', 35, -1, common.DEFAULT_FONT, 14 )
		level_label.anchorX = 0
		level_label.anchorY = 0.5
		level_label:setFillColor( utils.color(109, 107, 98) )

		local level_number = display.newText( button_group, data.level, 80, -3, common.DEFAULT_FONT, 24 )
		level_number.anchorX = 0
		level_number.anchorY = 0.5
		level_number:setFillColor( utils.color(109, 107, 98) )

		local locked_icon = display.newImageRect( button_group, 'images/locked.png', 23, 23 )
		locked_icon.anchorX = 1
		locked_icon.x = screenW - 25
		locked_icon.y = 0
	end

	return button_group
end

function open_game( level )
	composer.gotoScene( 'game', {
		params = {
			level = level,
		} 
	})
end

local function show_ad_box( height )
	main_group.y = screenH - 5 * 52 - 15 - height - 1
	background_footer.alpha = 1
	background_footer.y = screenH - height
end

local function hide_ad_box(  )
	main_group.y = screenH - 5 * 52 - 15
	background_footer.alpha = 0
end

local function update_sound_button()
	if settings.sounds then
		sounds_button.text = ''
	else
		sounds_button.text = ''
	end
end

-- Called when the scene's view does not exist =
function scene:create( event )
	local sceneGroup = self.view

	-- display a background image
	local background = common.background()
	sceneGroup:insert( background )
	
	timer.performWithDelay( 1000, function ( ... )
		local w = 320 / (720 / 320)
		local h = 348 / (720 / 320)
		local titleLogo = display.newImageRect( sceneGroup, 'images/logo.jpg', w, h )
		titleLogo.x = halfW
		titleLogo.y = h * 0.5

		titleLogo.xScale = 10
		titleLogo.yScale = 10

		transition.scaleTo( titleLogo, {
			xScale = 1,
			yScale = 1,
			time = 150
		} )
	end)

	-- fb button
	local fb = common.button({
		x = 23,
		y = 20,
		w = 36,
		h = 30,
		label = '',
		font = common.AWESOME_FONT,
		label_offset = 1,
		font_size = 18,
		handler = function ( ... )
			sounds.play(sounds.sound.button_click)

			ga:view('facebook')

			if (not system.openURL( 'fb://profile/1320778024609334/' )) then
				system.openURL( 'https://www.facebook.com/bigsloganquiz/' )
			end
		end
	})
	sceneGroup:insert( fb )

	-- leaderboard button
	local leaderboard = common.button({
		x = screenW - 23,
		y = 20,
		w = 36,
		h = 30,
		label = '',
		font = common.AWESOME_FONT,
		handler = function ( ... )
			sounds.play(sounds.sound.button_click)

			ga:view('leaderboards')
			game_network.show_leaderboards()
		end
	})
	sceneGroup:insert( leaderboard )

	-- sounds button
	sounds_button = display.newEmbossedText({
		text = '',
		font = common.AWESOME_FONT,
		fontSize = 24,
		x = screenW - 34,
		y = 68,
	})
	sounds_button.anchorX = 0
	sounds_button:setFillColor( utils.color( common.RED_COLOR ) )
	sceneGroup:insert( sounds_button )

	utils.handlerAdd( sounds_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( sounds_button )
		elseif event.phase == 'ended' and common.button_up( sounds_button ) then
			if settings.sounds then
				settings.sounds = false
			else
				settings.sounds = true
			end

			utils.saveSettings( settings )

			update_sound_button()
		end

		return true
	end )

	-- main group will be shifted by ad
	main_group = display.newGroup()
	sceneGroup:insert( main_group )

	table = widget.newTableView{
		left = 0, top = 0,
		width = screenW, height = 5 * 52,
		hideBackground = true,
		rowTouchDelay = 500,
		onRowTouch = nil,
		onRowRender = function ( event )
			row = event.row

			local level_element
			local level_stats = settings.stats[row.index]
			local level_settings = slogan_levels[row.index]

			if level_stats then
				level_element = game_level_button({
					level = row.index,
					unlocked = level_stats.unlocked,
					successfull = level_stats.successfull,
					time = level_stats.time,
					score = level_stats.score,
					stats_text = level_stats.stats .. ' of ' .. #level_settings,
					handler = open_game,
				})
			else
				level_element = game_level_button({
					level = row.index,
					unlocked = false,
					successfull = false,
				})
			end

			level_element.y = 26

			if slogan_levels[row.index].animated ~= true and row.index <= 5 then
				level_element.x = -screenW

				local tm = timer.performWithDelay( 100 + 50 * row.index, function ( event )
					transition.to( level_element, {
						x = 0,
						transition = easing.outBack,
					} )

					slogan_levels[event.source.params.index].animated = true
				end )

				tm.params = {
					index = row.index
				}
			end

			row:insert( level_element )
		end
	}
	main_group:insert(table)

	-- levels
	for i = 1, #slogan_levels do
		local rowHeight = 52
	    local rowColor = { default={ 1, 0.5, 0, 0 }, over={ 1, 1, 1, 0.2 } }
	    local lineColor = { 233 / 255, 223 / 255, 184 / 255, 0.1 }

	    table:insertRow({
	    	rowHeight = rowHeight,
            rowColor = rowColor,
            lineColor = lineColor,
            data = {
            	i = i
        	}
	    })
	end

	background_footer = common.background_footer()
	sceneGroup:insert( background_footer )
end

function scene:show( event )
	if event.phase == 'will' then
		ga:view('menu')
		
		common.hide_ad_box_callback = hide_ad_box
		common.show_ad_box_callback = show_ad_box

		ads.hide()
		hide_ad_box()

		if settings.ads_removed == true then
		else
			ads.show( 'banner', { x = 0, y = 10000 } )
		end

		if reload then
			table:reloadData()
		end
		reload = true

		update_sound_button()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == 'will' then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)
	elseif event.phase == 'did' then
		hide_ad_box()
	end	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to =
function scene:destroy( event )
	local group = self.view
	
end

-- Listener setup
scene:addEventListener( 'create', scene )
scene:addEventListener( 'show', scene )
scene:addEventListener( 'hide', scene )
scene:addEventListener( 'destroy', scene )

return scene