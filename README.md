# Big SLogan Quiz

Full source code and assets of Big Slogan Quiz game.

The game is released for [Android](https://play.google.com/store/apps/details?id=org.kneego.bigsloganquiz) and [iOS](https://itunes.apple.com/us/app/big-slogan-quiz/id1161246778?ls=1&mt=8) and you can try it.

## Requirements

* [Corona SDK](https://coronalabs.com)

## Files

* **main.lua** - app entry point
* **menu.lua** - menu screen
* **game.lua** - game screen
* **complete.lua** - screen show after level is finished
* **common.lua** - common settings and methods
* **resources.lua** - resources helper allows to access data from `values` folder
* **sounds.lua** - sounds helper

## Settings

### build.settings

* iOS
	* CFBundleIdentifier
	* CFBundleDisplayName
	* CFBundleVersion
* Android
	* googlePlayGamesAppId - for google play

### config.lua

* Android
	* licence -> google -> key - for google play

### values/arrays.lua

* ads settings
* score boards settings
* achievements settings
