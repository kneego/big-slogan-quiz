-----------------------------------------------------------------------------------------
--
-- game.lua
--
-----------------------------------------------------------------------------------------

local composer = require( 'composer' )
local scene = composer.newScene()

local common = require( 'common' )

--------------------------------------------

local unlock_treshold = 13

-- forward declarations and other locals
local level  -- selected level
local current_slogan
local progress
local progress_value
local sceneGroup
local main_group
local answer_buttons
local answer_button_size
local level_number
local progress_number
local time_text
local timers
local background_footer

local slogan_text_parts
local answer_1, answer_2, answer_3
local answers
local shuffled_slogans
local correct_answer
local correct_answer_position
local correct_answer_count

local total_time
local start_time
local total_score

function upload_score()
	local score_sum = 0
	for i = 1, #settings.stats do
		score_sum = score_sum + (settings.stats[i].score or 0)
	end

	if score_sum > 0 then 
		game_network.send_score(R.arrays('board_id')[platform_name], score_sum)
	end

	if score_sum > 100 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['100_points'])
	end

	if score_sum > 1000 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['1000_points'])
	end
end

function unlock_achievements()
	if settings.total_time > 10 * 60000 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['10_minutes_in_game'])
	end

	if settings.total_time > 30 * 60000 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['30_minutes_in_game'])
	end

	if settings.total_time > 60 * 60000 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['1_hour_in_game'])
	end
end

function unlock_level_finished_achievement()
	if level == 1 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['1st_level_complete'])
	elseif level == 5 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['5_levels_complete'])
	elseif level == 10 then
		game_network.unlock_achievement(R.arrays('achievements')[platform_name]['10_levels_complete'])
	end
end

function answer_button(answer_id, size, logo_path, handler)
	local x = 20 + (answer_id - 1) * size + (4 * answer_id - 1)

	local button_group = display.newGroup( )

	local shadow = display.newRoundedRect( x + 1, screenH - size - 24, size, size, 5 )
	shadow:setFillColor( 0.8 )
	shadow.alpha = 0.6
	shadow.anchorX = 0
	shadow.anchorY = 0
	button_group:insert( shadow )

	local button = display.newRoundedRect( x, screenH - size - 25, size, size, 5 )
	button.anchorX = 0
	button.anchorY = 0
	button_group:insert( button )

	local logo = display.newImageRect( button_group, 'images/logos/' .. logo_path, size - 20, size - 20 )
	logo.anchorX = 0
	logo.anchorY = 0
	logo.x = x + 10
	logo.y = screenH - size - 15

	utils.handlerAdd( button_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( button_group )
		elseif event.phase == 'ended' and common.button_up( button_group ) then
			handler( answer_id )
		end

		return true
	end )

	return button_group
end

function create_progress()
	local progress_group = display.newGroup()

	progress = {}
	for i = 0, 360 do
		progress[i] = display.newRect( progress_group, halfW, 0, 35, 1 )
		progress[i].anchorX = 0
		progress[i]:rotate(i - 90)
		progress[i]:setFillColor( utils.color( common.RED_COLOR ) )
	end

	local blue_circle = display.newCircle( progress_group, halfW, 0, 30 )
	blue_circle:setFillColor( utils.color(101, 169, 161) )

	return progress_group
end

function set_progress( percent )
	for i = 0, 360 do
		if (i / 360) * 100 < percent then
			progress[i].alpha = 1
		else
			if progress[i].alpha == 1 then
				transition.to( progress[i], {
					alpha = 0,
					time = 150
				} )
			end
		end
	end
end

function set_progress_number()
	progress_number.text = current_slogan .. '/' .. #shuffled_slogans
end

function progressbar_timer_handler()
	if progress_value >= 0 then
		set_progress(progress_value)
		progress_value = progress_value - 2
		table.insert(timers, timer.performWithDelay( 100,  progressbar_timer_handler ))
	else
		sounds.play(sounds.sound.wrong)

		end_game()
	end
end

function animate_answer_button( button )
	button.y = screenH + 100
	
	transition.to( button, {
		time = 400,
		y = 0,
		transition = easing.outBack,
	} )
end

function hide_answers( delay )
	delay = delay or 400

	-- buttons off
	transition.to( answer_1, {
		time = delay,
		y = screenH + 100,
	} )

	transition.to( answer_2, {
		time = delay,
		y = screenH + 100,
	} )

	transition.to( answer_3, {
		time = delay,
		y = screenH + 100,
	} )
end

function hide_slogan( delay )
	delay = delay or 400

	for i = 1, #slogan_text_parts do
		transition.to( slogan_text_parts[i], {
			alpha = 0,
			time = delay,
			onComplete = function ()
				display.remove( slogan_text_parts[i] )
			end
		} )
	end
end

function show_slogan()
	slogan_text_parts = {}

	local slogan_text = get_slogan( correct_answer ).slogan
	local width = 0

	for i = 1, #slogan_text do
		slogan_text_parts[i] = display.newText({
			text = slogan_text[i],
			font = common.DEFAULT_FONT,
			fontSize = 36,
			x = halfW,
			y = 150 + ((screenH - 150 - answer_button_size + main_group.y) / 2) - (#slogan_text * 20) + ((i - 1) * 40),
			align = 'center',
		})
		slogan_text_parts[i]:setFillColor( utils.color(common.RED_COLOR) )
		slogan_text_parts[i].alpha = 0
		sceneGroup:insert( slogan_text_parts[i] )

		if slogan_text_parts[i].width > width then
			width = slogan_text_parts[i].width
		end

		transition.to( slogan_text_parts[i], {
			alpha = 1,
			time = 800,
		} )
	end

	scale = 1
	if width > screenW - 20 then
		scale = (screenW - 20) / width
	end

	for i = 1, #slogan_text do
		slogan_text_parts[i].xScale = scale
		slogan_text_parts[i].yScale = scale

		transition.to( slogan_text_parts[i], {
			alpha = 1,
			time = 800,
		} )
	end
end

function answer_button_handler( answer_id )
	if answer_id == correct_answer_position then
		correct_answer_count = correct_answer_count + 1
	end

	end_game()
end

function show_answers()
	-- buttons on scene
	answer_button_size = ((screenW - 40 - 12) / 3)

	answer_1 = answer_button(1, answer_button_size, get_slogan(answers[1]).id .. '.png', answer_button_handler)
	main_group:insert( answer_1 )
	animate_answer_button( answer_1 )

	answer_2 = answer_button(2, answer_button_size, get_slogan(answers[2]).id .. '.png', answer_button_handler)
	main_group:insert( answer_2 )
	animate_answer_button( answer_2 )
	
	answer_3 = answer_button(3, answer_button_size, get_slogan(answers[3]).id .. '.png', answer_button_handler)
	main_group:insert( answer_3 )
	animate_answer_button( answer_3 )
end

function shuffle()
	-- randomize
	shuffled_slogans = {}
	local available_slogans = table.copy( slogan_levels[level] )

	while (#available_slogans > 0) do
		local random_slogan_position = math.random( 1, #available_slogans )
		table.insert( shuffled_slogans, table.remove( available_slogans, random_slogan_position ).id)
	end
end

function is_in_array(item, array)
	for j = 1, #array do
		if item == array[j] then
			return true
		end
	end

	return false
end

function get_slogan(id)
	for i = 1, #all_slogans do
		if all_slogans[i].id == id then
			return all_slogans[i]
		end
	end
end

function get_correct_answer()
	correct_answer = shuffled_slogans[ current_slogan ]
	utils.debug_json(get_slogan(correct_answer))
end

function get_random_answers()
	answers = {}

	local _category = get_slogan(correct_answer).category

	for i = 0, 3 do
		while true do
			local _answer = slogan_categories[_category][ math.random( 1, #slogan_categories[_category] ) ].id

			if _answer ~= correct_answer and not is_in_array( _answer, answers ) then
				-- we have new unique item
				table.insert( answers, _answer )
				break
			end
		end
	end
end

function set_correct_answer_position( )
	correct_answer_position = math.random( 1, 3 )
	answers[correct_answer_position] = correct_answer
end

function start_level()
	-- shuffle slogans
	shuffle()

	-- start from slogan at first position
	current_slogan = 1

	correct_answer_count = 0
	total_score = 0

	-- total time calculating
	start_time = system.getTimer()
	timer.performWithDelay( 10, update_time, -1 )

	start_game()
end

function stop_level()
	-- update total time in game
	settings.total_time = settings.total_time + total_time
	utils.saveSettings(settings)

	-- unlock achievements
	unlock_achievements()
end

function cancel_timers()
	for i = 1, #timers do
		timer.cancel( timers[i] )
	end
end

function start_game()
	cancel_timers()

	progress_value = 100
	set_progress(100)
	set_progress_number()

	-- select correct answer
	get_correct_answer()

	-- select 3 random logos from selected level and category
	get_random_answers()

	-- set position of the right answer
	set_correct_answer_position()

	show_answers()

	table.insert(timers, timer.performWithDelay( 500, function ( ... )
		-- show slogan
		show_slogan()

		progressbar_timer_handler()
	end ))
end

function end_game()
	-- move to next slogan
	current_slogan = current_slogan + 1

	cancel_timers()

	hide_answers()

	-- hide old slogan
	hide_slogan()

	-- what to do next
	table.insert(timers, timer.performWithDelay( 400, function()
		if (current_slogan > #shuffled_slogans) then
			-- stop current level
			stop_level()

			local _score = math.round((100 - math.round(total_time / 1000)) / 15 * correct_answer_count)
			if _score < 0 then _score = 0 end

			-- unlock next level
			unlocked = false
			if correct_answer_count >= unlock_treshold then
				if level < #slogan_levels and settings.stats[level + 1] == nil then
					settings.stats[level + 1] = {
						unlocked = true,
						successfull = false,
						time = 0,
						score = 0,
						stats = 0
					}
					
					settings.stats[level].successfull = true
					utils.saveSettings(settings)

					unlocked = true

					unlock_level_finished_achievement()
				end

				-- save stats
				if total_time < settings.stats[level].time or settings.stats[level].time == 0 then
					-- new best total time
					settings.stats[level].time = total_time
					utils.saveSettings(settings)
				end

				total_score = level * _score
				if total_score > (settings.stats[level].score or 0) then
					settings.stats[level].score = total_score
					utils.saveSettings(settings)

					-- upload to game network
					upload_score()
				end
			end

			if correct_answer_count > settings.stats[level].stats then
				-- new best score
				settings.stats[level].stats = correct_answer_count
				utils.saveSettings(settings)
			end

			ga:event('game', 'end_game', 'level', level)
			ga:event('game', 'end_game', 'total_time', total_time)
			ga:event('game', 'end_game', 'correct_answer_count', correct_answer_count)

			-- level complete
			composer.gotoScene( 'complete', {
				params = {
					level = level,
					total_time = total_time,
					correct_answer_count = correct_answer_count,
					total_score = total_score,
					unlocked = unlocked,
				}
			})
		else
			-- next question
			start_game()
		end
	end ))
end

function quit_game()
	stop_level()
	cancel_timers()
	transition.cancel() 

	hide_slogan( 0 )
	hide_answers( 0 )

	composer.gotoScene( 'menu' )
end

function update_time( event )
	total_time = (system.getTimer() - start_time)
	time_text.text = common.format_time(total_time)
end

local function show_ad_box( height )
	main_group.y = -height - 1

	background_footer.alpha = 1
	background_footer.y = screenH - height
end

local function hide_ad_box(  )
	main_group.y = 0
	background_footer.alpha = 0
end

-- Called when the scene's view does not exist =
function scene:create( event )
	sceneGroup = self.view

	-- display a background image
	local background = common.background()
	sceneGroup:insert( background )

	-- quit button
	local quit = common.button({
		x = 55,
		y = 20,
		w = 100,
		h = 30,
		label = 'QUIT LEVEL',
		label_offset = -1,
		font = common.MENU_FONT,
		font_size = 14,
		handler = quit_game
	})
	sceneGroup:insert( quit )

	local clock_icon = display.newImageRect( sceneGroup, 'images/clock-red.png', 23, 27 )
	clock_icon.x = screenW - 60
	clock_icon.y = 20

	time_text = display.newText( {
		text = '',
		font = common.MENU_FONT,
		fontSize = 14,
		x = screenW - 28,
		y = 20
	} )
	time_text:setFillColor( utils.color( common.RED_COLOR ) )
	sceneGroup:insert( time_text )

	level_number = display.newText( {
		text = '',
		font = common.ITALIC_FONT,
		fontSize = 18,
		x = halfW,
		y = 80
	} )
	level_number:setFillColor( utils.color(109, 107, 98) )
	level_number.alpha = 0.8
	sceneGroup:insert( level_number )

	local progress = create_progress()
	progress.y = 135
	sceneGroup:insert( progress )

	set_progress(100)

	progress_number = display.newText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 18,
		x = halfW,
		y = 135 - 2
	} )
	progress_number:setFillColor( utils.color( common.WHITE_COLOR ) )
	sceneGroup:insert( progress_number )

	-- main group will be shifted by ad
	main_group = display.newGroup()
	sceneGroup:insert( main_group )

	slogan_text_parts = {}

	background_footer = common.background_footer()
	sceneGroup:insert( background_footer )
end

function scene:show( event )
	if event.phase == 'will' then
		ga:view('game')

		level = 1
		if event.params then 
			-- init levelu
			level = event.params.level
		end

		-- set level number
		level_number.text = 'Level ' .. level

		common.hide_ad_box_callback = hide_ad_box
		common.show_ad_box_callback = show_ad_box

		ads.hide()
		hide_ad_box()

		if settings.ads_removed == true then
		else
			ads.show( 'banner', { x = 0, y = 10000 } )
		end

		timers = {}
	elseif event.phase == 'did' then
		start_level()
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == 'will' then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)
	elseif event.phase == 'did' then
		hide_ad_box()
	end	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to =
function scene:destroy( event )
	local group = self.view
	
end

-- Listener setup
scene:addEventListener( 'create', scene )
scene:addEventListener( 'show', scene )
scene:addEventListener( 'hide', scene )
scene:addEventListener( 'destroy', scene )

return scene