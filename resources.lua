local _strings = require( "values.strings" )
local _arrays = require( "values.arrays" )

local function strings( name )
	if _strings[language] ~= nil and _strings[language][name] ~= nil then
		return _strings[language][name]
	end

	return _strings['default'][name]
end

local function arrays( name )
	if _arrays[language] ~= nil and _arrays[language][name] ~= nil then
		return _arrays[language][name]
	end

	return _arrays['default'][name]
end

return {
	strings = strings,
	arrays = arrays
}