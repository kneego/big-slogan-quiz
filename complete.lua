-----------------------------------------------------------------------------------------
--
-- complete.lua
--
-----------------------------------------------------------------------------------------

local composer = require( 'composer' )
local scene = composer.newScene()

local common = require( 'common' )

--------------------------------------------

-- forward declarations and other locals
local main_group
local main_group_unlocked
local level_number
local complete_label
local time_stats
local answer_stats
local score_stats
local background_footer

local level
local total_time
local correct_answer_count
local total_score
local next_level_button
local next_level_disabled_button
local slogan_count_label
local stats_screen_group
local unlocked_label
local unlocked_label_effect


function stats_item( data )
	local stats_item_group = display.newGroup()

	local icon = display.newImageRect( stats_item_group, 'images/' .. data.icon, data.icon_width, data.icon_height )
	icon.x = -100
	icon.y = 20

	transition.to( icon, {x = 60, time = 300} )

	local label = display.newText({
		text = data.label,
		font = common.ITALIC_FONT,
		fontSize = 18,
		x = screenW,
		y = 0
	})
	label:setFillColor( utils.color( common.GRAY_COLOR ) )
	label.anchorX = 0
	label.alpha = 0.8
	stats_item_group:insert( label )

	transition.to( label, {x = 90, time = 300} )

	local value = display.newText({
		text = data.value,
		font = common.DEFAULT_FONT,
		fontSize = 22,
		x = 90,
		y = 20,
	})
	value.alpha = 0
	value.anchorX = 0
	value:setFillColor( 0 )
	stats_item_group:insert( value )

	local top = display.newText({
		text = '(TOP ' .. data.top .. ')',
		font = common.DEFAULT_FONT,
		fontSize = 22,
		x = 0,
		y = 20,
	})
	top.alpha = 0
	top.x = 84 + value.width + 10
	top.anchorX = 0
	top:setFillColor( utils.color( common.BLUE_COLOR ) )
	stats_item_group:insert( top )

	timer.performWithDelay( 400, function()
		transition.to( value, {alpha = 1, time = 400} )
		transition.to( top, {alpha = 1, time = 400} )
	end )

	return stats_item_group
end

local function show_ad_box( height )
	main_group.y = -height - 1
	main_group_unlocked.y = -height - 1

	background_footer.alpha = 1
	background_footer.y = screenH - height
end

local function hide_ad_box(  )
	main_group.y = 0
	main_group_unlocked.y = 0

	background_footer.alpha = 0
end

local function init_ad()
	if settings.ads_removed == true then
	else
		ads.show( 'banner', { x = 0, y = 10000 } )
	end
end

-- Called when the scene's view does not exist =
function scene:create( event )
	local sceneGroup = self.view

	-- display a background image
	local background = common.background()
	sceneGroup:insert( background )

	-- menu button
	local menu = common.button({
		x = 38,
		y = 20,
		w = 66,
		h = 30,
		label = 'MENU',
		label_offset = -1,
		font = common.MENU_FONT,
		font_size = 14,
		handler = function ( ... )
			composer.gotoScene( 'menu' )
		end
	})
	sceneGroup:insert( menu )

	-- stats screen
	stats_screen_group = display.newGroup()
	sceneGroup:insert( stats_screen_group )

	local background_width = 720
	local background_height = 180
	local height = (background_height / background_width) * screenW
	local background_red = display.newImageRect( stats_screen_group, 'images/background-red.jpg', screenW, height )
	background_red.x = halfW
	background_red.y = 42
	background_red.anchorY = 0

	-- level label
	level_number = display.newText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 24,
		x = 84,
		y = 68
	} )
	level_number.anchorX = 0
	level_number:setFillColor( utils.color( 255, 255, 255 ) )
	stats_screen_group:insert( level_number )

	-- complete label
	complete_label = display.newText( {
		text = 'Complete',
		font = common.DEFAULT_FONT,
		fontSize = 24,
		x = 84,
		y = 92
	} )
	complete_label.anchorX = 0
	complete_label:setFillColor( utils.color( 255, 255, 255 ) )
	stats_screen_group:insert( complete_label )

	-- main group will be shifted by ad
	main_group = display.newGroup()
	stats_screen_group:insert( main_group )

	-- try again button
	local try_again_button = common.button({
		x = halfW - 55 - 5,
		y = screenH - 50,
		w = 110,
		h = 50,
		label = 'TRY AGAIN',
		label_offset = -1,
		font = common.MENU_FONT,
		font_size = 14,
		fill = 'blue',
		handler = function ( ... )
			composer.gotoScene( 'game', {
				params = {
					level = level,
				}
			})
		end
	})
	main_group:insert( try_again_button )

	-- next level button
	next_level_button = common.button({
		x = halfW + 55 + 5,
		y = screenH - 50,
		w = 110,
		h = 50,
		label = 'NEXT LEVEL',
		label_offset = -1,
		font = common.MENU_FONT,
		font_size = 14,
		handler = function ( ... )
			composer.gotoScene( 'game', {
				params = {
					level = level + 1,
				}
			})
		end
	})
	main_group:insert( next_level_button )

	-- next level disabled button
	next_level_disabled_button = display.newGroup()
	main_group:insert(next_level_disabled_button)

	local next_level_disabled_border = display.newRoundedRect( halfW + 55 + 5, screenH - 50, 110, 50, 5 )
	next_level_disabled_border:setFillColor( 0, 0, 0, 0 )
	next_level_disabled_border.strokeWidth = 1
	next_level_disabled_border:setStrokeColor( 0.8 )
	next_level_disabled_border.alpha = 0.4
	next_level_disabled_button:insert( next_level_disabled_border )

	local you_need_all = display.newText( {
		text = 'You need',
		font = common.ITALIC_FONT,
		fontSize = 16,
		x = halfW + 55 + 5,
		y = screenH - 58
	} )
	you_need_all:setFillColor( utils.color( common.GRAY_COLOR ) )
	next_level_disabled_button:insert( you_need_all )

	slogan_count_label = display.newText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 18,
		x = halfW + 55 + 5,
		y = screenH - 42
	} )
	slogan_count_label:setFillColor( utils.color( common.GRAY_COLOR ) )
	next_level_disabled_button:insert( slogan_count_label )

	-- level unlocked screen
	level_unlocked_screen = display.newGroup()
	sceneGroup:insert( level_unlocked_screen )

	local unlocked_image = display.newImageRect( level_unlocked_screen, 'images/unlocked.png', 67, 100 )
	unlocked_image.x = halfW
	unlocked_image.y = halfH - 60

	unlocked_label = display.newText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 24,
		x = halfW,
		y = halfH + 30,
		align = 'center'
	} )
	unlocked_label:setFillColor( utils.color( common.BLUE_COLOR ) )
	level_unlocked_screen:insert( unlocked_label )

	unlocked_label_effect = display.newText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 24,
		x = halfW,
		y = halfH + 30,
		align = 'center'
	} )
	unlocked_label_effect:setFillColor( utils.color( common.BLUE_COLOR ) )
	level_unlocked_screen:insert( unlocked_label_effect )

	-- main group will be shifted by ad
	main_group_unlocked = display.newGroup()
	level_unlocked_screen:insert( main_group_unlocked )

	local next_level_button = common.button({
		x = halfW,
		y = screenH - 50,
		w = screenW - 50,
		h = 50,
		label = 'CONTINUE',
		label_offset = -1,
		font = common.MENU_FONT,
		font_size = 14,
		handler = function ( ... )
			stats_screen_group.alpha = 1
			level_unlocked_screen.alpha = 0

			init_ad()
		end
	})
	main_group_unlocked:insert( next_level_button )

	background_footer = common.background_footer()
	stats_screen_group:insert( background_footer )
end

function scene:show( event )
	local sceneGroup = self.view

	if event.phase == 'will' then
		ga:view('complete')

		level = 1
		correct_answer_count = 0
		total_time = 0
		total_score = 0
		unlocked = false

		if event.params then 
			-- init levelu
			level = event.params.level
			correct_answer_count = event.params.correct_answer_count
			total_time = event.params.total_time
			total_score = event.params.total_score
			unlocked = event.params.unlocked
		end

		level_number.text = 'Level ' .. level

		unlocked_label.text = 'Level ' .. level + 1 .. '\nunlocked!'
		unlocked_label_effect.text = 'Level ' .. level + 1 .. '\nunlocked!'

		level_number.alpha = 0
		complete_label.alpha = 0
		unlocked_label.alpha = 0

		transition.to( level_number, {alpha = 1, time = 200} )
		transition.to( complete_label, {alpha = 1, time = 200} )
		transition.to( unlocked_label, {alpha = 1, time = 200} )

		-- time stats
		time_stats = stats_item({
			icon = 'clock-red.png',
			icon_width = 23,
			icon_height = 27,
			label = 'Your Time',
			value = common.format_time(total_time),
			top = common.format_time(settings.stats[level].time)
		})
		time_stats.y = 160
		stats_screen_group:insert( time_stats )

		-- answer stats
		timer.performWithDelay( 200, function()
			answer_stats = stats_item({
				icon = 'check-red.png',
				icon_width = 25,
				icon_height = 22,
				label = 'Correct Answers',
				value = correct_answer_count .. ' of ' .. #slogan_levels[ tonumber(level) ],
				top = settings.stats[level].stats
			})
			answer_stats.y = 220
			stats_screen_group:insert( answer_stats )
		end)

		slogan_count_label.text = 13 .. ' of ' .. #slogan_levels[ tonumber(level) ]

		-- score stats
		timer.performWithDelay( 400, function()
			score_stats = stats_item({
				icon = 'badge-red.png',
				icon_width = 16,
				icon_height = 26,
				label = 'Level Score',
				value = total_score,
				top = settings.stats[level].score or 0
			})
			score_stats.y = 280
			stats_screen_group:insert( score_stats )
		end)

		if settings.stats[level + 1] == nil then
			-- next level is locked
			sounds.play(sounds.sound.wrong)
			
			next_level_button.alpha = 0
			next_level_disabled_button.alpha = 0.4
		else
			next_level_button.alpha = 1
			next_level_disabled_button.alpha = 0
		end

		common.hide_ad_box_callback = hide_ad_box
		common.show_ad_box_callback = show_ad_box

		ads.hide()
		hide_ad_box()
		
		if unlocked then
			-- new level unlocked
			sounds.play(sounds.sound.top_score)

			stats_screen_group.alpha = 0
			level_unlocked_screen.alpha = 1

			-- animation
			unlocked_label_effect.xScale = 1
			unlocked_label_effect.yScale = 1
			unlocked_label_effect.alpha = 1

			transition.to( unlocked_label_effect, {
				xScale = 4,
				yScale = 4,
				alpha = 0,
				time = 800,
			} )
		else
			stats_screen_group.alpha = 1
			level_unlocked_screen.alpha = 0

			init_ad()
		end
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == 'will' then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)
	elseif phase == 'did' then
		display.remove( time_stats )
		display.remove( answer_stats )
		display.remove( score_stats )
	end	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to =
function scene:destroy( event )
	local group = self.view
	
end

-- Listener setup
scene:addEventListener( 'create', scene )
scene:addEventListener( 'show', scene )
scene:addEventListener( 'hide', scene )
scene:addEventListener( 'destroy', scene )

return scene