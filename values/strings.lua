local _M = {}

_M.default = {
	numerix = 'Numerix',
	
	game_over = 'Game Over',

	facebook_url = 'https://m.facebook.com/numerixgame/',
	twitter_url = 'https://m.twitter.com/KneeGoApps',

	new_best_score = 'New top score!',
	score = 'Score:',
	your_best = 'Your best:',

	please_wait = 'Please wait...',

	description = 'is a combination\nof a math game\nand a teaching app.',
 
	share_text_1 = 'My score in math game ',
	share_text_2 = ' is ',
	share_text_3 = ' points.\nCan you beat me?\n\n',
	share_link = 'http://www.kneego.org/numerix/',
}

if is_android then
	_M.default.numerix = 'Numerino'
end

_M.sk = {
	new_best_score = 'Nove top skore!',
	score = 'Skore:',
	your_best = 'Najlepsie:',

	share_text_1 = 'Moje score v matematickej hre ',
	share_text_2 = ' je ',
	share_text_2 = ' bodov.\nPrekonas ma?\n\n',
}

_M.cs = {
    new_best_score = 'Nove top skore!',
    score = 'Skore:',
	your_best = 'Nejlepsi:',

	share_text_1 = 'V matematicke hre ',
	share_text_2 = ' mam score ',
	share_text_3 = ' bodu.\nPrekonas me?\n\n',
}

return _M