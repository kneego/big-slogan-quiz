local _M = {
	default = {},
	sk = {},
	cs = {},
}

_M.default.admob_id_banner = {
	['iOS'] = 'ca-app-pub-1210274935500874/7238583943',
	['Android'] = 'ca-app-pub-1210274935500874/2808384346'
}

_M.default.admob_id_interstitial = {
	['iOS'] = 'ca-app-pub-1210274935500874/1192050345',
	['Android'] = 'ca-app-pub-1210274935500874/4285117541'
}

_M.default.admob_id_own = {
	['iOS'] = 'ca-app-pub-1210274935500874/8575716341',
	['Android'] = 'ca-app-pub-1210274935500874/2529182746'
}

_M.default.board_id = {
	['iOS'] = 'bigsloganquiz_topscore',
	['Android'] = 'CgkIk5Cy57sPEAIQAQ',
}

_M.default.achievements = {
	['iOS'] = {
		['1st_level_complete'] = '70397906',
		['100_points'] = '70397907',
		['1000_points'] = '70398058',
		['5_levels_complete'] = '70398059',
		['10_levels_complete'] = '70398060',
		['10_minutes_in_game'] = '70398061',
		['30_minutes_in_game'] = '70398062',
		['1_hour_in_game'] = '70398063',
	},
	['Android'] = {
		['1st_level_complete'] = 'CgkIk5Cy57sPEAIQAg',
		['100_points'] = 'CgkIk5Cy57sPEAIQAw',
		['1000_points'] = 'CgkIk5Cy57sPEAIQBw',
		['5_levels_complete'] = 'CgkIk5Cy57sPEAIQBA',
		['10_levels_complete'] = 'CgkIk5Cy57sPEAIQBQ',
		['10_minutes_in_game'] = 'CgkIk5Cy57sPEAIQBg',
		['30_minutes_in_game'] = 'CgkIk5Cy57sPEAIQCA',
		['1_hour_in_game'] = 'CgkIk5Cy57sPEAIQCQ',
	},
}

_M.default.slogans = require( 'values.slogans' )

return _M
